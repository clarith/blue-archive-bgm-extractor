import os
import re
from mitmproxy import ctx

def write_file(file_name, content):
  with open(file_name, "wb") as f:
    f.write(content)

def save_file(req_path, content):
  match = re.search(r"(.+)/(.+?)$", req_path)
  dir_name = match.group(1)
  file_name = match.group(2)
  os.makedirs("resources" + dir_name, exist_ok=True)
  write_file("resources" + req_path, content)

def response(flow):
  if flow.request.host == "prod-clientpatch.bluearchiveyostar.com" or flow.request.host == "prod-notice.bluearchiveyostar.com":
    ctx.log.info('< %s' % (flow.request.path))
    save_file(flow.request.path, flow.response.content)
